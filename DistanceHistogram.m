function DistanceHistogram(layer, pltOpt,prtOpt)

    nLayer = size(layer,2);
    
    widthHist = []; 
    distHist = []; 
    
    %obtain a list of all distances between points, in the same order as width
    %of each point
    for layer_num = 1:nLayer
        
        ncourses = size(layer(layer_num).Course,2);
        
        for course_num = 1:ncourses 
            if (course_num == ncourses)&& prtOpt.frameBool
                continue %shouldn't account for perimeter of the image as a toolpath
            else
                x = layer(layer_num).Course(course_num).POINT(:,1); 
                y = layer(layer_num).Course(course_num).POINT(:,2); 
                width = layer(layer_num).Course(course_num).POINT(:,4);
        
                D_first = [x(1:end-1) y(1:end-1)]; 
                D_second = [x(2:end) y(2:end)]; 
                try 
                    dist = sqrt( (D_second(:,1) - D_first(:,1)).^2 + (D_second(:,2) - D_first(:,2)).^2 ); 
                    widthHist = [widthHist; width(2:end)];
                    distHist = [distHist; dist];
                catch 
                    disp(course_num)
                end
            end
        end
    
    
            % create bins of specific width 
            bin_upper = ceil(max(widthHist)*10)/10; 
            bins = [0:bin_upper/30:bin_upper];
            id = cell(length(bins)-1);  %will hold all the ids for each wdith range
            mm_concerned = ones(1,length(bins)) ;
            for i=2:length(bins)
                 %find all the indices of widthHist for which width belongs to bins(i)
                logicalIndexes = (widthHist > bins(i-1)) & (widthHist < bins(i));
            %     % Get means where row is in range
            %     meanValue = mean(widthHist(logicalIndexes));
                % You don't need the actual index numbers but if you want them, you can get them with find():
                id{i-1} = find(logicalIndexes);
                % get distance of printed lines concerned (in mm) by the line
                % width range in "bin"
                mm_concerned(i-1) =sum(distHist(id{i-1})); 
            end
    
    %create barplot
        if strcmp(pltOpt.plot_histDistance, 'yes')
        figure; 
        hold on; 
        b = bar(bins(2:end), 100*mm_concerned(2:end)./sum(distHist), 'BarWidth', 1,  'FaceColor','#4DBEEE');
        set(gca, 'FontName', 'Myriad Pro', 'Fontsize', 16)
        xlabel('Line Width (mm)')
        ylabel('Aggregate line length (%)')
        box on
        axis square
            if strcmpi(pltOpt.save_plots, 'yes')
                figname = strcat(pltOpt.foldername,'HistogramLinewidth_', string(layer_num));
                savefig(figname); 
                saveas(gcf,figname,'png')
            end
        end
    end

end