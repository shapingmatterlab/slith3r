close all 
clear all


%% Preferences


boolExample = 0 ;  %1 to load example, 0 to load your own parameters
if boolExample == 1
    [ANGLES,dim,prtOpt,pltOpt] = chooseExample(boolExample);
else
    %Load your own parameters here, generated in parametersInput
    loadfolder = './STARRYNIGHT_outputs/'; 
    load(strcat(loadfolder,'ANGLES.mat')); % ANGLES FORMAT EXPECTED: Struct with fields 'U' 'V', 'X', 'Y'
    load(strcat(loadfolder,'DIM.mat')); %[dimx dimy]; 
    load(strcat(loadfolder,'prtOpt.mat')); % Structure defining toolpath and printing-related parameters in parametersInput.m
    load(strcat(loadfolder,'pltOpt.mat')); % Structure defining plotting-related parameters in parametersInput.m
end

if ~exist(pltOpt.foldername, 'dir')
   mkdir(pltOpt.foldername)
end

   
%% create streamlines
if strcmpi(pltOpt.plot_quiver, 'yes')
    PlotQuiver(pltOpt,ANGLES)
end 

Nmem = 100;    
integStep = 0.15; 

if strcmp(prtOpt.ControlSeedPoint,'yes')
    LAYER = regularStreamlines(ANGLES, prtOpt, integStep, Nmem,pltOpt); 
else
    LAYER = evenStreamlines(ANGLES, prtOpt,integStep, Nmem,pltOpt); 
end


if strcmpi(pltOpt.save_matlabfiles, 'yes')
     save(pltOpt.filename_LAYERstruct, 'LAYER')
end 

%% Streamlines to Print paths 

% Trim points outside shape 
if prtOpt.frameBool == 1 
    a_SL = gca; 
    layer = CutShape(LAYER, ANGLES, dim, prtOpt) ;
else layer = LAYER;
end

%% Reduce amount of points within one path (avoid overload of printer
% buffer), remove paths if too short and offset by x_printstart and
% y_printstart
layerSimplify = SimplifyPaths(layer, prtOpt,pltOpt); 

plotGcode(layerSimplify)


% Reorder courses for printing (minimize travel length between closest extremities)
layerMoved = movePrint(layerSimplify,[prtOpt.x_printstart prtOpt.y_printstart],1);

layerReorder = ReorderPaths(layerMoved,prtOpt);


if strcmpi(pltOpt.save_matlabfiles, 'yes')
    save(strcat(pltOpt.filename_LAYERstruct, '_reordered'), 'layerReorder');
end

% Statistics- visualize distribution of line widths 
DistanceHistogram(layerReorder,pltOpt,prtOpt);

% Create gcode
if strcmpi(pltOpt.save_matlabfiles, 'yes')
     GCodeFormat(layerReorder,prtOpt);
end
