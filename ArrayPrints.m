%% array prints
close all

BLcorner = [20 30]; %x and y coordinates of bottom left corner
spacing = 25 ; %[mm] of distance between starting points along y
nbRepeats = 3;

prtOpt.x_printstart = 120; %[mm] %value to add to all X values for translation 
prtOpt.y_printstart = 60 ; %35 60 %[mm] %value to add to all Y values for translation 

fname =  'SENT_Spiral_3p5_nooffset'; 
pltOpt.foldername = strcat(fname,'_outputs/');
prtOpt.gcode_fname = strcat(pltOpt.foldername, fname,'_BW_', string(prtOpt.bandwidth*1000).replace('.','p'),sprintf('_X%d_Y%d',prtOpt.x_printstart,prtOpt.y_printstart) ,'.gcode');
prtOpt.gcodeExtremes_foldername = './begin_endGcodes/'; %where beginning & end gcode files can be found, to be changed with printer

% Reorder courses for printing (minimize travel length between closest extremities)
layerMoved2 = movePrint(layerSimplify,[prtOpt.x_printstart prtOpt.y_printstart],0);

layerReorder2 = ReorderPaths_debug(layerMoved2,0,prtOpt);

% Create gcode
GCodeFormat(layerReorder2,prtOpt);


% for i=1:nbrepeats
%        layerMoved = movePrint(layerReorder,[BLcorner(1) BLcorner(2) + (i-1)*spacing],0);
% 
% end