G11
;END
G1 E-0.10000 F2400.000
M907 E538 ; reset extruder motor current
M104 S0 ; turn off temperature
M140 S0 ; turn off heatbed
M107 ; turn off fan
G1 Z39.02 ; Move print head up
G1 X0 Y200 F3000 ; home X axis
M84 ; disable motors