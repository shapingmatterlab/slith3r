;FLAVOR:MARLIN

; ACCELERATION SPEED FEEDRATES
M201 X1000 Y1000 Z200 E5000 ; sets maximum accelerations, mm/sec^2
M203 X200 Y200 Z12 E120 ; sets maximum feedrates, mm/sec
M204 P1250 R1250 T1250 ; sets acceleration (P, T) and retract acceleration (R), mm/sec^2
M205 X8.00 Y8.00 Z0.40 E4.50 ; sets the jerk limits, mm/sec
M205 S0 T0 ; sets the minimum extruding and travel feed rate, mm/sec

; RETRACTION PARAMETERS 
;set R feedrate according to Yannick's guideline
M207 S1 F1200 ;sets retraction settings for G10/G11

; GENERAL PARAMETERS
G90 ; use absolute coordinates
M83 ; extruder relative mode
G21 ; set units to millimeters
M907 E538 ; set extruder motor current for lower speeds and layer heights to avoid heat creep and preserve motor life- default value is E538
M221 S100 ; reset flow

; LCP Specific 
M104 S300 ; set extruder temp
M140 S90 ; set bed temp
;M900 K0.03 Filament gcode LA 1.5

; GET PRINTER READY 
M190 S90 ; wait for bed temp
M109 S300 ; wait for extruder temp
G28 W ; home all without mesh bed level
G80 ; mesh bed leveling


; PRIMER 
G1 Y-3.0 F1000.0 ; go outside print area
G92 E0.0
G1 X60.0 E9.0 F1000.0 ; intro line
G1 X100.0 E12.5 F1000.0 ; intro line
G92 E0.0
G1 E-0.20000 F2100.00000
G1 Z0.600 F10800.000
G92 E0.0